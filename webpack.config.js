const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

var mode = process.env.NODE_ENV || 'production';
module.exports = {
    performance: {
        hints: false,
    },
    mode: mode,
    entry: path.join(__dirname, "src", "index.js"),
    output: {
        path: path.join(__dirname, "/dist"), // the bundle output path
        filename: "bundle.js", // the name of the bundle
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "public/index.html", // to import index.html file inside index.js
        })
    ],
    devServer: {
        port: 3031, // you can change the port
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/, // .js and .jsx files
                exclude: /node_modules/, // excluding the node_modules folder
                use: {
                    loader: "babel-loader",
                },

            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(sa|sc|c)ss$/, // styles files
                use: ["style-loader", "css-loader", "sass-loader"],
            },



            {
                test: /\.(png|svg)$/, // to import images
                type: 'asset/resource',
            }
        ],
    },
};