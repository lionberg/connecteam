import * as React from "react";
import {ReactElement, useEffect, useRef, useState} from "react";

interface InputProps {
    children?       : any,
    text?           : string,
    hint?           : string,
    isHintVisible?   : boolean,
    cssCss?          : string,
    isActive?        : boolean,
    linesNumber?    : number,
    isOptions?: boolean
}
export interface IState{
    text?           : string,
    isOptions: boolean
}

function CTInput(props:InputProps) {
    const wrapperRef = useRef(null);
    const [width, setWidth] = useState(0);
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event:any) {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setState({...state, isOptions : false})
        }
    }
    useEffect(() => {

        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);

    useEffect(() => {
        console.log(width);
        wrapperRef.current.parentElement.parentElement.nextElementSibling.width = wrapperRef.current.offsetWidth;
       setWidth(wrapperRef.current.parentElement.offsetWidth);
    }, );
    let s:InputProps;
     s = {...s,...props};
    const [state, setState] = useState(props);

        let p;
        if (state.cssCss !== undefined && state.cssCss.indexOf('combo') >= 0){
            p = (<div onClick={() => { setState({...state, isOptions : !state.isOptions}) }}
                      className={'div-pointer' + (!state.isOptions ? '' : ' rotate')}></div>);
        }

        let input;
        if (props.linesNumber !== undefined && props.linesNumber > 1){

            input = <div className={'ct-input ' + props.cssCss}>
                <div className={'hint'}>
                    {props.hint}
                </div>
                <div>
                    <textarea onChange={e => {

                        setState({...state, text: e.target.value});
                       // this.setState({text: e.target.value})
                    }} className={props.isActive ? 'active' : 'inactive'} value={state.text}/>
                </div>
                {p}
            </div>;
        } else {
            input = <div className={'ct-input ' + props.cssCss}>
                <div className={'hint'}>
                    {props.hint}
                </div>
                <div>
                    <input onChange={e => {
                        setState({...state, text: e.target.value} );
                    }} className={props.isActive ? 'active' : 'inactive'} value={state.text}/>
                </div>
                {p}
            </div>;
        }

        return (<div ref={wrapperRef}>
                    {input}
                        <div style={{width: width, position: "absolute"}}>
                            { state.isOptions ? props.children : <></>}
                        </div>
                </div>);
    }

export default CTInput;