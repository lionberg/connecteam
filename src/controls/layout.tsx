import * as React from 'react';
import {FunctionComponent, ReactElement, useEffect, useState} from "react";
import LinksMenu from "./LinksMenu";
import Section1 from "./Pages/Section1";
import Section2 from "./Pages/Section2";
import Section3 from "./Pages/Section3";
import Section4 from "./Pages/Section4";
import Section5 from "./Pages/Section5";
import {Header} from "./header";

interface ILayout {
    children? : any;
}

function Layout(props: ILayout)  {
        return (<React.Fragment>
            <div className={'ct_layout_wrapper'}>
                <div className={'ct_content'}>
                    <Header></Header>
                </div>
                <div className={'ct_link_menu_section'}>
                    <LinksMenu></LinksMenu>
                </div>
                <div className={'display-flex full-width'}>
                    <div className={'ct-layout-left-col'}></div>
                    <div className={'ct-layout-middle-col'}>
                        <Section1></Section1>
                    </div>
                    <div className={'ct-layout-right-col'}></div>
                </div>
                <div className={'display-flex full-width'}>
                    <div className={'ct-layout-left-col'}></div>
                    <div className={'ct-layout-middle-col'}>
                        <Section2></Section2>
                    </div>
                    <div className={'ct-layout-right-col'}></div>
                </div>
                <div className={'display-flex full-width'}>
                    <div className={'ct-layout-middle-col margin-top-m'}>
                        <Section3></Section3>
                    </div>

                </div>
                <div className={'display-flex full-width desktop-margin-top-base'}>
                    <div className={'ct-layout-left-col'}></div>
                    <div className={'ct-layout-middle-col'}>
                        <Section4></Section4>
                    </div>
                    <div className={'ct-layout-right-col'}></div>
                </div>
                <div
                    className={'display-flex full-width padding-top-base padding-bottom-base bg-color-neutral-2 margin-top-m'}>
                    <div className={'ct-layout-left-col'}></div>
                    <div className={'ct-layout-middle-col'}>
                        <Section5></Section5>
                    </div>
                    <div className={'ct-layout-right-col'}></div>
                </div>


            </div>
        </React.Fragment>)
}

export default Layout;


