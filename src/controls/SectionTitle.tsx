import * as React from 'react';
import {ReactElement} from "react";

export interface TitleProps {
    description: string,
    title: string,
    icon: ReactElement,
    children?:any
}

const SectionTitle = (props: TitleProps) => {
    return <div className={'gap-s display-flex align-items-center ct-section-title'}>
            <div>
                {props.icon}
            </div>
            <div className={'flex1'}>
                <div className={'ct-desc'}>
                    {props.description}
                </div>
                <div className={'ct-title'}>
                    {props.title}
                </div>
            </div>
    </div>
}

export default SectionTitle;