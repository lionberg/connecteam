import * as React from "react";
import {LinkWithIcon, Svg} from "@lionberg/connecteam_library";
import {svg1, svg2, svg3, svg4} from './Pages/SvgLinkIcon'
import {JSONPath} from 'jsonpath-plus'
import {useEffect} from "react";
import {debug} from "util";

function LinkMenu() {

    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/home.json');
    let jsonObj = JSON.parse(obj);

    let _links = JSONPath({path: "$.content.items.*", json : jsonObj}) || [];

    useEffect(() => {

    });

    let getIcon = (index:number) => {
        switch (index){
            case 0:
                return svg1();
                break;
            case 1:
                return svg2();
                break;
            case 2:
                return svg3();
                break;
            case 3:
                return svg4();
                break;
        }
    }
    let links= () => {
    }


    let div =
        <div className={'display-flex align-items-center gap-s'}>
            <div className={'flex2'}/>

            {(Array.from(_links || [])as any[]).map((l: { name: string; }, index:  number) =>

                {
                    return (
                    <div key={'l_'+index} className={'flex1 link'+(index+1)}>
                        <LinkWithIcon label={l.name} leftIcon={getIcon(index)}></LinkWithIcon>
                    </div>
                )}
            )
            }
            <div className={'flex2'}/>
        </div>
    ;
    return div;

}

export default LinkMenu;