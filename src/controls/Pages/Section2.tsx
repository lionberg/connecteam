import * as React from "react";
import {LinkWithIcon, Svg} from "@lionberg/connecteam_library";
import SectionTitle from '../SectionTitle'
import {arrowSvg} from "./SvgLinkIcon";
import SectionImage from "../SectionImage";
import {section2SvgIcon} from "./SvgLinkIcon";
import {JSONPath} from "jsonpath-plus";


function Section2() {

    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/sit-at-enim.json');
    let jsonObj = JSON.parse(obj);

    const sectionSvg = () => {
        return <svg width="64" height="58" viewBox="0 0 64 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M25.6008 25.8001C32.67 25.8001 38.4007 20.0693 38.4007 13.0001C38.4007 5.93084 32.67 0.200073 25.6008 0.200073C18.5315 0.200073 12.8008 5.93084 12.8008 13.0001C12.8008 20.0693 18.5315 25.8001 25.6008 25.8001Z" fill="#00EEFF"/>
            <path d="M26.72 32.2324C17.984 31.9124 0 36.2644 0 45.0003V51.4003H30.528C22.624 42.5683 26.592 32.5524 26.72 32.2324Z" fill="#00EEFF"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M57.6 38.5999C57.6 40.9679 56.928 43.1759 55.776 45.0639L64 53.2879L59.488 57.7999L51.264 49.5759C49.376 50.6959 47.168 51.3999 44.8 51.3999C37.728 51.3999 32 45.6719 32 38.5999C32 31.5279 37.728 25.7999 44.8 25.7999C51.872 25.7999 57.6 31.5279 57.6 38.5999ZM38.4 38.5999C38.4 42.1199 41.28 44.9999 44.8 44.9999C48.32 44.9999 51.2 42.1199 51.2 38.5999C51.2 35.0799 48.32 32.1999 44.8 32.1999C41.28 32.1999 38.4 35.0799 38.4 38.5999Z" fill="#00EEFF"/>
        </svg>


    }

    return <div className={'display-flex align-items-center full-width flex-wrap ct-section-content gap-sl'}>
        <div className={'flex1 section-image-col phone-full-width phone-margin-top-base'}>
            <SectionImage icon={section2SvgIcon()} cssClass={'ct-image-section-2'} corner={'left-bottom'}></SectionImage>
        </div>
        <div className={'flex3'}>

            <div className={'ct-section-text margin-top-m'}>
                <div className={'link2'}>
                    <SectionTitle description={JSONPath({path: "$.label", json : jsonObj})} title={JSONPath({path: "$.title", json : jsonObj})} icon={sectionSvg()}></SectionTitle>
                </div>
                <div className={'padding-top-base'} dangerouslySetInnerHTML={{__html: JSONPath({path: "$.description", json : jsonObj})}}>
                </div>
                <div className={'margin-top-m link2'}>
                    <LinkWithIcon label={JSONPath({path: "$.heroLink.label", json : jsonObj})} icon={arrowSvg()}></LinkWithIcon>
                </div>

            </div>

        </div>

    </div>

}

export default Section2;