import * as React from "react";
import {LinkWithIcon, Svg} from "@lionberg/connecteam_library";
import SectionTitle from '../SectionTitle'
import {arrowSvg} from "./SvgLinkIcon";
import SectionImage from "../SectionImage";
import {JSONPath} from "jsonpath-plus";

function Section4() {

    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/praesentium-aspernatur.json');
    let jsonObj = JSON.parse(obj);

    const sectionSvg = () => {
        return <svg width="64" height="52" viewBox="0 0 64 52" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M57.6 6.80015H32L25.6 0.400146H6.4C2.88 0.400146 0 3.28015 0 6.80015V45.2C0 48.72 2.88 51.6 6.4 51.6H57.6C61.12 51.6 64 48.72 64 45.2V13.2001C64 9.68015 61.12 6.80015 57.6 6.80015ZM51.008 42.0001L41.6 36.4961L32.192 42.0001L34.688 31.3441L26.4 24.1761L37.312 23.2481L41.6 13.2001L45.888 23.2481L56.8 24.1761L48.512 31.3441L51.008 42.0001Z" fill="#FF007A"/>
        </svg>


    }

    return <div className={'display-flex align-items-center full-width flex-wrap ct-section-content phone-flex-revert-columns gap-sl'}>
        <div className={'flex3'}>

            <div className={'ct-section-text margin-top-m'}>
                <div className={'link4'}>
                    <SectionTitle description={JSONPath({path: "$.label", json: jsonObj})} title={JSONPath({path: "$.title", json: jsonObj})} icon={sectionSvg()}></SectionTitle>
                </div>
                <div className={'padding-top-base'} dangerouslySetInnerHTML={{__html: JSONPath({path: "$.description", json: jsonObj})}}>
                    </div>
                <div className={'margin-top-m link4'}>
                    <LinkWithIcon label={JSONPath({path: "$.heroLink.label", json: jsonObj})} icon={arrowSvg()}></LinkWithIcon>
                </div>

            </div>

        </div>
        <div className={'flex1 section-image-col phone-full-width'}>
            <SectionImage icon={sectionSvg()} cssClass={'ct-image-section-4'} corner={'right-bottom'}></SectionImage>
        </div>
    </div>

}

export default Section4;