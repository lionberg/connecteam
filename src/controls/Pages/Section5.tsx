import * as React from "react";
import {arrowSvg} from "./SvgLinkIcon";
import CTInput from "../CTInput";
import {Button, LinkWithIcon} from "@lionberg/connecteam_library/dist/esm";
import {JSONPath} from "jsonpath-plus";
import {useEffect, useRef, useState} from "react";
import {ButtonGroup} from "@lionberg/connecteam_library";
import {ButtonGroupItem} from "@lionberg/connecteam_library";


interface IFooter {
    btnValue?: number,
    selectedOption?: number,
    selectedOptionText?: string,
    isOptions? : boolean;
}

function Section5(props:IFooter) {
    let initState:IFooter;
    const ref = useRef(null);

    const [state, setState] = useState({...initState, ...props});
    const [width, setWidth] = useState(0);


    useEffect(() => {
        //console.log(ref);
        //setWidth(ref.current.parentElement.parentElement.offsetWidth);
    }, []);
    //setState({...state, btnValue: 0});
    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/footer.json');
    let jsonObj = JSON.parse(obj);
    let _options = JSONPath({path: "$.form.fields[?(@.type='options' && @.name === 'Eius distinctio nobis')].options", json: jsonObj});
    if (_options === null || _options === undefined){
        _options = [];
    }

    let textareaHint = (JSONPath({path: "$.form.fields[?(@.type==='textarea')].name", json: jsonObj}) || [])[0];
    const sectionSvg = () => {
        return <svg width="64" height="52" viewBox="0 0 64 52" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M57.6 6.80015H32L25.6 0.400146H6.4C2.88 0.400146 0 3.28015 0 6.80015V45.2C0 48.72 2.88 51.6 6.4 51.6H57.6C61.12 51.6 64 48.72 64 45.2V13.2001C64 9.68015 61.12 6.80015 57.6 6.80015ZM51.008 42.0001L41.6 36.4961L32.192 42.0001L34.688 31.3441L26.4 24.1761L37.312 23.2481L41.6 13.2001L45.888 23.2481L56.8 24.1761L48.512 31.3441L51.008 42.0001Z" fill="#FF007A"/>
        </svg>
    }

    return <div className={'display-flex full-width flex-wrap gap-sl'}>
        <div className={'section-5-left-col'}>
            <div className={'ct-section-text'}>
            <div className={'heading2 color-main'}>
                {JSONPath({path: "$.intro.title", json: jsonObj})}
            </div>
            <div className={'margin-top-m font-size-base'} dangerouslySetInnerHTML={{__html:JSONPath({path: "$.intro.text", json: jsonObj})}}>
            </div>
            <div className={'margin-top-m color-main'}>
                <LinkWithIcon label={JSONPath({path: "$.intro.link.label", json: jsonObj})} icon={arrowSvg()}/>
            </div>
            </div>
        </div>
        <div className={'section-5-right-col'}>
            <div className={'ct-section-text'} key={'key'+JSONPath({path: "$.form.fields[1].name", json: jsonObj})}>
                <div className={'font-size-base'}>
                    {JSONPath({path: "$.form.title", json: jsonObj})}
                </div>
                <div className={'margin-top-s display-flex gap-s full-width flex-wrap small-flex-direction-column phone-flex-direction-column'}>
                    <div className={'flex1'}>
                        <CTInput isHintVisible={true}
                                 text={JSONPath({path: "$.form.fields[0].name", json: jsonObj})}
                                 cssCss={'full-width ct-input-bold ct-active'}
                                 isActive={false}></CTInput>
                    </div>
                    <div className={'flex1'}>
                        <CTInput isHintVisible={true}
                                 text={JSONPath({path: "$.form.fields[1].name", json: jsonObj})}
                                 cssCss={'full-width ct-active'}
                                 isActive={true}
                                 hint={JSONPath({path: "$.form.fields[1].name", json: jsonObj})}></CTInput>
                    </div>

                </div>
                <div className={'margin-top-s display-flex gap-s full-width  flex-wrap small-flex-direction-column phone-flex-direction-column'}>
                    <div className={'flex1'}>
                        <CTInput isHintVisible={true}
                                 hint={JSONPath({path: "$.form.fields[2].name", json: jsonObj})}
                                 cssCss={''}
                                 text={JSONPath({path: "$.form.fields[2].name", json: jsonObj})}
                                 isActive={false}></CTInput>
                    </div>
                    <div className={'flex1'}>
                        <CTInput key={'combo_'+ Math.random()} isHintVisible={true}
                                 text={state.selectedOptionText}
                                 cssCss={'full-width ct-active ct-input-bold combo'}
                                 isOptions={state.isOptions}
                                 isActive={false}>
                                {
                                    options()
                                }
                        </CTInput>
                    </div>
                </div>
                <div className={'display-flex gap-s full-width'} key={Math.random()}>
                    <div className={'flex1 margin-top-m'}>
                        <ButtonGroup>
                            {
                                getButtons().map((button: any, index: number) => {

                                    return (

                                        <ButtonGroupItem value={button.value}
                                                         text={button.label}
                                                         isSelected={state.btnValue===button.value}
                                        onclick={groupBtnOnClick}></ButtonGroupItem>
                                    )
                                })
                            }


                        </ButtonGroup>
                    </div>
                </div>
                <div className={'display-flex gap-s full-width margin-top-m'}>

                    <div className={'flex1'} key={Math.random()}>
                        <div></div>
                        <CTInput linesNumber={2}
                                 isHintVisible={true}
                                 hint={textareaHint}
                                 cssCss={''}
                                 text={''} isActive={false}></CTInput>
                    </div>
                </div>
                <div className={'display-flex gap-s full-width margin-top-m'}>
                    <div className={'flex1'}>
                        <Button label={JSONPath({path: "$.form.submitLabel", json: jsonObj})} isSecondary={false} icon={arrowSvg()}></Button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    function getButtons() {
        let btns = JSONPath({path: "$.form.fields[?(@.layout === 'buttons')].options", json: jsonObj});
        if (btns !== undefined) {
            return btns[0];
        } else {
            return Array.from([]) as any[];
        }
    }

    function getOptions(){
        let opts = JSONPath({path: "$.form.fields[?(@.type='options' && @.name === 'Eius distinctio nobis')].options", json: jsonObj});
        if (opts !== undefined) {
            return opts[0];
        } else {
            return Array.from([]) as any[];
        }
    }

    function options(){
        return <div  ref={ref} className={'options'} key={Math.random()}>
            {getOptions().map((option:any, index:number) =>{
            return <div onClick={() => {
                setState({...state, selectedOption: option.value, selectedOptionText : option.label, isOptions : false})
            }} className={'option'} key={index}>{option.label}</div>
        })}
        </div>
    }
    function groupBtnOnClick(value:number){
        console.log(value);
        setState({...state, btnValue:value});
    }
}

export default Section5;