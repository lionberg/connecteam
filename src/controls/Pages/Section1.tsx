import * as React from "react";
import {LinkWithIcon, Svg} from "@lionberg/connecteam_library";
import SectionTitle from '../SectionTitle'
import {arrowSvg} from "./SvgLinkIcon";
import SectionImage from "../SectionImage";
import {section1SvgIcon} from "./SvgLinkIcon";
import {JSONPath} from "jsonpath-plus";


function Section1()  {

    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/repudiandae.json');
    let jsonObj = JSON.parse(obj);

    const sectionSvg = () => {
        return <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="60" height="59.9999" rx="30" fill="#F8EFFF"/>
            <path d="M30 12C20.064 12 12 20.064 12 30C12 39.9359 20.064 47.9999 30 47.9999H39V44.3999H30C22.188 44.3999 15.6 37.8119 15.6 30C15.6 22.188 22.188 15.6 30 15.6C37.812 15.6 44.4 22.188 44.4 30V32.574C44.4 33.996 43.122 35.3999 41.7 35.3999C40.278 35.3999 39 33.996 39 32.574V30C39 25.032 34.968 21 30 21C25.032 21 21 25.032 21 30C21 34.9679 25.032 38.9999 30 38.9999C32.484 38.9999 34.752 37.9919 36.372 36.3539C37.542 37.9559 39.558 38.9999 41.7 38.9999C45.246 38.9999 48 36.1199 48 32.574V30C48 20.064 39.936 12 30 12ZM30 35.3999C27.012 35.3999 24.6 32.988 24.6 30C24.6 27.012 27.012 24.6 30 24.6C32.988 24.6 35.4 27.012 35.4 30C35.4 32.988 32.988 35.3999 30 35.3999Z" fill="#7A00DA"/>
        </svg>

    }

    return <div className={'display-flex align-items-center full-width flex-wrap ct-section-content phone-flex-revert-columns gap-sl'}>
        <div className={'flex3'}>

            <div className={'ct-section-text margin-top-m'}>
                <div className={'link1'}>
                    <SectionTitle description={JSONPath({path: "$.label", json : jsonObj})} title={JSONPath({path: "$.title", json : jsonObj})} icon={sectionSvg()}></SectionTitle>
                </div>
                <div className={'padding-top-base'} dangerouslySetInnerHTML={{__html : JSONPath({path: "$.description", json : jsonObj})}}>

                </div>
                <div className={'margin-top-m link1'}>
                    <LinkWithIcon label={JSONPath({path: "$.heroLink.label", json : jsonObj})} icon={arrowSvg()}></LinkWithIcon>
                </div>

            </div>

        </div>
        <div className={'flex1 section-image-col phone-full-width'}>
            <SectionImage icon={section1SvgIcon()} cssClass={'ct-image-section-1'} corner={'right-bottom'}></SectionImage>
        </div>
    </div>

}

export default Section1;