import * as React from "react";
import {arrowSvg} from "./SvgLinkIcon";
import {Button} from "@lionberg/connecteam_library/dist/esm";
import {JSONPath} from "jsonpath-plus";


function Section3() {

    let obj = localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/dolore-ipsum.json');
    let jsonObj = JSON.parse(obj);
    let labels = JSONPath({path: "$.items", json : jsonObj}) || [];

    const sectionSvg = () => {
        return <svg width="120" height="120" viewBox="0 0 120 120" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="120" height="120" rx="60" fill="#0098DA"/>
            <path d="M96 27.6001L89.988 33.6121L84.012 27.6001L78 33.6121L71.988 27.6001L66.012 33.6121L60 27.6001L53.988 33.6121L48.012 27.6001L42 33.6121L35.988 27.6001L30.012 33.6121L24 27.6001V85.2001C24 89.1601 27.24 92.4001 31.2 92.4001H88.8C92.76 92.4001 96 89.1601 96 85.2001V27.6001ZM56.4 85.2001H31.2V63.6001H56.4V85.2001ZM88.8 85.2001H63.6V78.0001H88.8V85.2001ZM88.8 70.8001H63.6V63.6001H88.8V70.8001ZM88.8 56.4001H31.2V45.6001H88.8V56.4001Z" fill="white"/>
        </svg>;
    }

    const userCheck = () => {
        return <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.57889 9.02431L5.05257 11.5001L7.57889 14.0264H-6.10352e-05V12.3422C-6.10352e-05 10.1022 4.48836 8.97379 6.73678 8.97379C7.00626 8.97379 7.25047 8.99063 7.57889 9.02431Z" fill="#0098DA"/>
            <path d="M10.1052 3.92115C10.1052 5.78221 8.59783 7.28957 6.73678 7.28957C4.87573 7.28957 3.36836 5.78221 3.36836 3.92115C3.36836 2.0601 4.87573 0.552734 6.73678 0.552734C8.59783 0.552734 10.1052 2.0601 10.1052 3.92115Z" fill="#0098DA"/>
            <path d="M7.57904 11.5L10.5011 14.4474L16.0001 8.89794L14.8211 7.71057L10.5011 12.0643L8.75799 10.3127L7.57904 11.5Z" fill="#0098DA"/>
        </svg>

    }

    const wifiSvg = () => {
        return <svg width="16" height="15" viewBox="0 0 72 73" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 13.2945V0.199341C39.7481 0.199341 72 32.4513 72 72.1991H58.9049C58.9049 39.6698 32.5295 13.2945 0 13.2945Z" fill="black"/>
            <path d="M0 39.4847V26.3896C25.3111 26.3896 45.8098 46.8883 45.8098 72.1991H32.7146C32.7146 54.1531 18.0463 39.4847 0 39.4847Z" fill="black"/>
            <path d="M20.1748 62.1117C20.1748 67.6827 15.6585 72.1989 10.0874 72.1989C4.51628 72.1989 0 67.6827 0 62.1117C0 56.5403 4.51628 52.0241 10.0874 52.0241C15.6585 52.0241 20.1748 56.5403 20.1748 62.1117Z" fill="black"/>
        </svg>

    }
    return <div className={'text-align-center section3 padding-bottom-base text-align-center'}>
        <div className={'ct_content_3'}>
            <div className={'margin-top-m'}>
                {sectionSvg()}
            </div>
            <div className={'ct-desc margin-top-s'}>
                {JSONPath({path: "$.title", json: jsonObj})}
            </div>
            <div className={'heading1'}>
                Dolore ipsum
            </div>
            <div className={'display-flex'}>
                <div className={'section-3-left-col'}></div>
                <div className={'section-3-mid-col'}>
                    <div className={'margin-top-l section-3-content'} dangerouslySetInnerHTML={{__html: JSONPath({path: "$.description", json: jsonObj})}}>

                    </div>
                    <div className={'margin-top-l'}>
                        <div className={'ct-line'}></div>
                    </div>
                    <div className={'display-inline-block'}>
                        {
                            /* I did not find these buttons in provided json */


                        }
                        <div className={'display-flex align-items-center gap-m margin-top-l flex-wrap no-wrap'}>
                        <div className={'phone-full-width'}>
                            <Button cssClass={'blue'} icon={arrowSvg()} label={'Voluptatem repudiandae et'} isSecondary={false}></Button>
                        </div>
                        <div className={'phone-full-width'}>
                            <Button cssClass={'blue'} label={'Et aperiam et est'} isSecondary={true} icon={arrowSvg()} leftIcon={wifiSvg()}></Button>
                        </div>
                        <div className={'phone-full-width'}>
                            <Button cssClass={'blue'} label={'Minima aut omnis'} isSecondary={true} icon={arrowSvg()} leftIcon={userCheck()}></Button>
                        </div>
                    </div>
                    </div>
                </div>
                <div className={'section-3-right-col'}></div>
            </div>

        </div>
        <div className={'section-3-background-wrapper'}>
            <div className={'section-3-background'}></div>
        </div>

    </div>

}

export default Section3;