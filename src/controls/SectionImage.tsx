import * as React from 'react';
import {ReactElement} from "react";

export interface SectionImageProps {
    icon: ReactElement,
    cssClass: string,
    corner: string,
    children?:any
}

const SectionImage = (props: SectionImageProps) => {
    return <div className={'ct-section-image-wrapper'}>
        <div className={'ct-section-image ' + props.cssClass}></div>
        <div className={'ct-section-image-icon ' +  props.corner}>
            <div className={'ct-section-image-icon-placeholder'}>
            {props.icon}
            </div>
        </div>
    </div>
}

export default SectionImage;