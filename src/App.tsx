
import '@lionberg/connecteam_library/dist/all.css'
import './App.css';
import * as React from 'react';
import Layout from './controls/layout'
import TextAPI, {ITextAPIProps} from "./api/TextAPI";
import {useEffect, useState} from "react";

let apiProps: ITextAPIProps;
let api: TextAPI;

interface IApp {
    canLoad? : boolean;
}

function  App (props:IApp){

    const [dom, setDom] = useState(false);

    apiProps = {

    };

    useEffect(() => {
        apiProps.onFetchedAll = ( () => {
                console.log('fetched all data');
                let json = JSON.parse(localStorage.getItem('https://connecteam.com/static/frontend-home-task/data/home.json'));
                if (JSON.stringify(dom) !== JSON.stringify(json))
                    setDom(json);
            }
        );
        api = new TextAPI(apiProps);
    },[]);


    return <div className="App">
      <Layout>

      </Layout>
    </div>;
}

export default App;
