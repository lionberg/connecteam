import * as React from "react";


export interface ITextAPIProps {
    apiUrl? : string,
    onFetched? : any,
    onFetchedAll?: any,
    children? :any
}

interface ITextAPIState {

}

let JSONArray = [
    {url: 'https://connecteam.com/static/frontend-home-task/data/home.json'},
    {url: 'https://connecteam.com/static/frontend-home-task/data/footer.json'},
    {url: 'https://connecteam.com/static/frontend-home-task/data/repudiandae.json'},
    {url: 'https://connecteam.com/static/frontend-home-task/data/sit-at-enim.json'},
    {url: 'https://connecteam.com/static/frontend-home-task/data/dolore-ipsum.json'},
    {url: 'https://connecteam.com/static/frontend-home-task/data/praesentium-aspernatur.json'}
]

class TextAPI extends React.Component<ITextAPIProps, ITextAPIState> {
    constructor(props: ITextAPIProps) {
        super(props);
        let itemNum = 0;
        JSONArray.map((elm, index) => {
            if (elm.url === undefined){
                return;
            }
            console.log('fetching from ' + elm.url);
            fetch(elm.url).then(response => response.json()).then(data => {
                //Global.apis[elm.url] = data;
                localStorage.setItem(elm.url, JSON.stringify(data));
                itemNum++;
                if (itemNum == JSONArray.length && this.props.onFetchedAll !== undefined){
                    this.props.onFetchedAll();
                }
            })
        });
    }
}

export default TextAPI;
