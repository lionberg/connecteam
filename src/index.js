import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

let rootElm = document.getElementById('root');
const root = ReactDOM.createRoot(rootElm);
function applyResolutionCss(){
    let width = window.outerWidth;
    if (width < 600) {

        rootElm.classList.remove('desktop');
        rootElm.classList.remove('small');
        if (!rootElm.classList.contains('phone'))
        {
            rootElm.classList.add('phone');
        }
    } else  {
        if (width < 1550 && width >= 600) {
            if (!rootElm.classList.contains('small'))
            {
                rootElm.classList.add('small');
            }
        } else {
            rootElm.classList.remove('small');
        }
        rootElm.classList.remove('phone');
        if (!rootElm.classList.contains('desktop')){
            rootElm.classList.add('desktop')
        }

    }
}
function onResizeHandler(){
    applyResolutionCss();
}
window.addEventListener('resize', onResizeHandler);
applyResolutionCss();

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
